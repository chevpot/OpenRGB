#include <linux/i2c.h>
#include <stdbool.h>
#include "openrgb.h"


static struct i2c_device_id openrgb_idtable[] = {
      { "openrgb", 0 },
      { }
} ;

static struct openrgb_device* openrgb_devices[16] ;

static const unsigned short normal_i2c[] = {
        0x20, 0x21, 0x22, 0x23,
        I2C_CLIENT_END };

MODULE_DEVICE_TABLE(i2c, openrgb_idtable);



static int openrgb_detect(struct i2c_client *client,
                          struct i2c_board_info *info)
{
    printk(KERN_INFO "Detect") ;
    struct i2c_adapter *adapter = client->adapter;

    if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE_DATA))
        return -ENODEV;
    
    bool pass = false ;
    int i = 0;

    for (i = 0xA0; i < 0xB0; i++)
    {
        printk(KERN_ALERT "%d\n", i) ;
        int res = i2c_smbus_read_byte_data(client, i);

        printk(KERN_DEBUG "%d\n", res) ;

        if (res != (i - 0xA0))
        {
            printk(KERN_DEBUG "Found device %d\n", client->addr) ;
            pass = true ;
            break ;
        }
    }

    if ( !pass )
        return -ENODEV;

    
    return 0 ;
}

/**
 *  File Operations struct for Lens control device
 */
struct file_operations open_rgb_fops = {
	.owner =    THIS_MODULE,
	.llseek =   no_llseek,
	.read =     0,
	.write =    0,
	.unlocked_ioctl = 0,
	.poll = 	open_rgb_poll,
	.open =     open_rgb_open,
	.release =  open_rgb_release,
};

static struct class *open_rgb_class;

static int open_rgb_setup_cdev(struct openrgb_device *openrgb_data)
{
	unsigned int first_minor = 1;
	int result = 0;
	dev_t char_dev;

    // Ask for a dynamic major device # and a range of minor device numbers
    // NOTE: The range of the minor # is 1.
    result = alloc_chrdev_region(&char_dev, first_minor, 1,"openrgb");
    if(result < 0)
    {
        printk(KERN_WARNING "open_rgb: Failed to allocate device numbers\n");
        return result;
    }

    // Initialize & add the char device to the system
	cdev_init(&openrgb_data->cdev, &open_rgb_fops);
	openrgb_data->cdev.owner = THIS_MODULE;
	openrgb_data->cdev.ops = &open_rgb_fops;
	result = cdev_add(&openrgb_data->cdev, char_dev, 1);

	// Fail gracefully if need be
	if(result < 0)
	{
		// Log error & return failure
		printk(KERN_INFO "open_rgb: Failed to add char device\n");
		return result;
	}

	// Create the char device node
	if(IS_ERR(device_create(open_rgb_class, 0, char_dev, 0, "openrgb")))
	{
		printk(KERN_ERR "open_rgb: Failed to create device, lens\n");
		return -1;
	}
	// All is well
	return 0;
}

static void open_rgb_remove_cdev(struct openrgb_device *openrgb_data)
{
	dev_t char_dev = openrgb_data->cdev.dev;

    // Release the dynamic device # and a range of minor device numbers
    // NOTE: The range of the minor # is 1.
    unregister_chrdev_region(char_dev, 1);

	// Remove the device node
	device_destroy(open_rgb_class, char_dev);

	// Remove the char device from the system
	cdev_del(&openrgb_data->cdev);
}

static int openrgb_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    printk(KERN_INFO "Probe") ;
    struct i2c_adapter *adapter = client->adapter;

    if (!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE_DATA))
        return -ENODEV;

    if((openrgb_data = kzalloc(sizeof(struct openrgb_device), GFP_KERNEL)) == 0)
	{
		// Failed
		return -ENOMEM;
	}

    i2c_set_clientdata(client, openrgb_data);
	openrgb_data->i2c_client = client;
    
    bool pass = true ;
    int i = 0;

    for (i = 0xA0; i < 0xB0; i++)
    {
        int res = i2c_smbus_read_byte_data(client, i);

        if (res != (i - 0xA0))
        {
            pass = false ;
            break ;
        }
    }

    if ( !pass )
        return -ENODEV;

    printk(KERN_INFO "Found device %02x\n", client->addr) ;

    open_rgb_class = class_create(THIS_MODULE, "openrgb");
	if (IS_ERR(open_rgb_class)) {
		printk(KERN_ERR "Couldn't create sysfs class\n");
		return PTR_ERR(open_rgb_class);
	}

    open_rgb_setup_cdev(openrgb_data);

    
    return 0 ;
}

static int openrgb_remove(struct i2c_client *client)
{
    printk(KERN_INFO "Remove") ;

    struct openrgb_device *openrgb_data = 0;

	// Get the devices private data
	openrgb_data = i2c_get_clientdata(client);

    open_rgb_remove_cdev(openrgb_data);

	// Remove class
	class_destroy(open_rgb_class);
	open_rgb_class = 0;

    kfree(openrgb_data);

    return 0 ;
}

static struct i2c_driver openrgb_driver = {
      .driver = {
              .name   = "openrgb",
              .owner  = THIS_MODULE,  /* optional */
      },

      .id_table       = openrgb_idtable,
      .probe          = openrgb_probe,
      .remove         = openrgb_remove,
      /* if device autodetection is needed: */
      .detect         = openrgb_detect,
      .address_list   = normal_i2c,
} ;

static int __init openrgb_init(void)
{
    printk(KERN_INFO "Loading openrgb module...\n");
    return i2c_add_driver(&openrgb_driver);
}

static void __exit openrgb_cleanup(void)
{
    printk(KERN_INFO "Unloading openrgb module...\n");
    i2c_del_driver(&openrgb_driver);
}

module_init(openrgb_init) ;
module_exit(openrgb_cleanup) ;

// module_i2c_driver(openrgb_driver);

MODULE_AUTHOR("Paul Topley <paultop6@outlook.com") ;
MODULE_DESCRIPTION("Driver to control i2c smbus for RGB devices.") ;

/* a few non-GPL license types are also allowed */
MODULE_LICENSE("GPL") ;