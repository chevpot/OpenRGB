#pragma once
#include <linux/cdev.h>

typedef struct openrgb_state
{
    int anything ;
} openrgb_state ;

struct openrgb_device
{
	struct mutex lock;						/**< Synchronization mutex */
    struct i2c_client *i2c_client;			/**< The I2c client for talking to the MSP430 */
    struct  cdev cdev;              		/**< Char device structure */
    openrgb_state update;							/**< Holds the devices update status information */
};